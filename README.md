# README

Ten skrypt Pythona używa biblioteki TensorFlow do treningu sieci neuronowej dla klasyfikacji obrazów. Skrypt jest zależny od pakietów `tensorflow`, `numpy`, `matplotlib.pyplot` oraz `time`.

## Definicja stałych i tensorboard

Na początku skryptu zdefiniowane są stałe, takie jak wysokość i szerokość obrazu (`img_height`, `img_width`) oraz wielkość batcha (`batch_size`). Skrypt tworzy również nowy folder do przechowywania logów dla TensorBoard, narzędzia do monitorowania treningu sieci neuronowej.

## Tworzenie modelu

Skrypt tworzy model sieci neuronowej, który składa się z kilku warstw. Pierwsza warstwa to warstwa przeskalowania, która przeskalowuje wartości pikseli z zakresu 0-255 do zakresu 0-1. Następnie mamy warstwę konwolucyjną, warstwę max pooling, warstwę spłaszczania oraz trzy warstwy gęsto połączone.

## Wczytanie danych

Dane są wczytywane z folderu 'dataset/img' za pomocą funkcji `image_dataset_from_directory()`. Funkcja ta zwraca `tf.data.Dataset` zawierający obrazy oraz odpowiadające im etykiety. Obrazy są automatycznie przeskalowane do wymiarów określonych przez `img_height` i `img_width`.

## Podział danych

Załadowane dane są następnie dzielone na trzy zbiory: treningowy, walidacyjny i testowy. Zbiory te są tworzone w proporcjach 80:10:10.

## Kompilacja i trening modelu

Model jest kompilowany za pomocą optymalizatora Adam, funkcji straty SparseCategoricalCrossentropy i metryki dokładności ("accuracy"). Następnie model jest trenowany na zestawie treningowym przez 15 epok, używając zbioru walidacyjnego do monitorowania postępu po każdej epoce.

## Ewaluacja modelu

Po zakończeniu treningu model jest ewaluowany na zbiorze testowym. Skrypt wypisuje dokładność modelu na tym zbiorze.

Podsumowując, ten skrypt jest narzędziem do treningu sieci neuronowej do klasyfikacji obrazów. Skrypt używa TensorFlow i Keras do budowy, treningu i ewaluacji modelu.
