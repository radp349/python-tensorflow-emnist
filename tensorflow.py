import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import TensorBoard
import time

# Deklaruje stałe
img_height = 28
img_width = 28
batch_size = 32

# Tworzę folder pod zbieranie logów, aby wyświetlic tensorboard
NAME = "literki-{}".format(int(time.time()))
tensorboard = TensorBoard(log_dir ='logs/{}'.format(NAME))

# Tworzę warstwy modelu
model = tf.keras.Sequential([
  tf.keras.layers.experimental.preprocessing.Rescaling(1./255),
  tf.keras.layers.Conv2D(32, 3, activation='relu'),
  tf.keras.layers.MaxPooling2D(),
  tf.keras.layers.Flatten(),
  tf.keras.layers.Dense(256, activation='relu'),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dense(40)
])

# Wczytuję obrazki do programu i ustawiam flagi 
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    'dataset/img',
    batch_size=batch_size,
    color_mode='rgb',
    image_size=(img_height, img_width),
    labels='inferred',
    label_mode= "int",
    seed=123,
    shuffle=True,
)


class_names = ['M', 'O', 'R', 'S', 'W',
               'b', 'c', 'f', 'i', 'p']

# Dokonuję podziału obrazów na datasety w proporcjach 80:10:10
image_count = len(train_ds)
train_size = int(0.8 * image_count)
val_size = int(0.1 * image_count)
test_size = int(0.1 * image_count)

train = train_ds.take(train_size)
test = train_ds.skip(train_size)
val = test.skip(test_size)
test = test.take(test_size)



model.compile(

    optimizer=keras.optimizers.Adam(),
    loss=[
        keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    ],
    metrics = ["accuracy"],
)

model.fit(train, epochs=15, validation_data=val, callbacks=[tensorboard])

print("Otrzymany rezultat: ")
model.evaluate(test)


